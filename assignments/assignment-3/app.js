// This looks great as well, Brett.  Your application
// works well.  It also tracks the number of wins and
// losses over time which wasn't required, but was a
// great feature (although a bit depressing because
// I consistently lost way more games than I won)
// Great work!
// 20/20


/* Handles game logic for all possible choices (IE: return 'win' if x is rock
   and y if scissors) the string that is returned will be used for comparison in
   the whoWins() function. */
var ROCK = (x, y) => (x == 'rock' && y == 'scissors') ? 'win' : 'lose';
var PAPER = (x, y) => (x == 'paper' && y == 'rock') ? 'win' : 'lose';
var SCISSORS = (x, y) => (x == 'scissors' && y == 'rock') ? 'lose' : 'win';
var DYNAMITE = (x, y) => (x == 'dynamite' && y == 'scissors') ? 'lose' : 'win';
var GAME = (x, y) => (x == 'scissors') ? SCISSORS(x, y) : (x == 'paper') ? PAPER(x, y) :
    (x == 'rock') ? ROCK(x, y) : (x == 'dynamite') ? DYNAMITE(x, y) : false;

/* Variables to track our stats */
var WINS = 0;
var LOSSES = 0;
var DRAWS = 0;
var GAMENUMBER = 1;

/* Array of choices for a user to choose */
var gamePieces = new Array("rock", "paper", "scissors", "dynamite");

function printStats() {
    /* A function to print our stats after a game has been completed */
    return 'Wins: ' + WINS + ' Losses: ' + LOSSES + " draws: " + DRAWS;
}

function initGame() {

    /* Main game loop, the do while loop handles verification and ensures
       consistent gameplay */
    var userChoice = ""
    do {
        userChoice = prompt((userChoice !== "" ? "Invalid choice. " : "") + "choose rock, paper, scissors or dynamite");
    } while (gamePieces.indexOf(userChoice) == -1);

    /* Randomly generate a choice for the computer based on our array */
    var cpuChoice = getRandomGamePiece(gamePieces);

    /* jQuery handling what gets displayed to our containers */
    $("#results").html("Game number: " + GAMENUMBER + "<br/>you chose: " + userChoice + "<br /> Computer chose: " + cpuChoice + "<br />" + whoWins(userChoice, cpuChoice));
    $("#stats").html(printStats())
}

function getRandomGamePiece(array) {
    /* Selects a random index from our array */
    return array[Math.floor(Math.random() * array.length)];
}

function whoWins(x, y) {
    /* Function to determine who wins and update our stats */
    if (x == y) {
        DRAWS += 1;
        GAMENUMBER += 1
        return "Game tied!";
    } else {
        if (GAME(x, y) == 'win') {
            WINS += 1;
            GAMENUMBER += 1
            return "You win!";
        } else if (GAME(x, y) == 'lose') {
            LOSSES += 1;
            GAMENUMBER += 1
            return "you lose!";
        }
    }
}
