//  Nice work, Brett.  Yep - A+
// Your application works great, all the validations work
// properly and you've got everything named correctly.
// I also see the comments I needed to see, so nothing wrong here
// 20/20

function loadProvinces(){
  /* An array of Provinces to be printed to the screen */
  var provArray = new Array("British Columbia", "Alberta", "Saskatchewan",
                            "Manitoba", "Ontario", "Quebec", "New Brunswick",
                            "Nova Scotia", "PEI", "Newfoundland", "NWT",
                            "Nunavut", "Yukon");

  /* The element we want to append our arrays to */
  var select = document.getElementById('cboProv');

  for(i=0; i<provArray.length; i++){
/*       While i's increment is less than the number of items in our Array
         Store the index of our current item in a variable
           EX: "Ontario" == provArray[4]
         And write our variable into our HTML with the specified tags
           output == <option value="Ontario">Ontario</option>                   */
    var prov = provArray[i];
    select.innerHTML += "<option value=\"" + prov + "\">" + prov + "</option>";
  }
}

function validateForm()
{

  // Setting objects to variables
  var name = document.getElementById('txtName');
  var email = document.getElementById('txtEmail');
  var prov = document.getElementById('cboProv');

/*    A regular expression for checking validity of Emails. This regular
      expression looks for a pattern of alpha-numerical characters dashes,
      underscores, and periods, followed by an "@" sign, followed by another
      string,  a period, and another string 2-5 characters long

      EX:       'some-email'    '@'  "some-Server"  "."    "com"              */
  var re = /^([a-zA-Z0-9\-\.\_]+)@([a-zA-Z0-9\-\.]+)\.([a-zA-Z]{2,5})$/;
  var properEmail = re.exec(email.value);

  if(name.value == "")
  {
    /* If object Value is empty
       Alert user to complete action
       Return focus to the object */
    alert('Please fill out your Name');
    return name.focus();
  }
  else if (email.value == "" || !properEmail)
  {
    alert('Please use a proper Email format (IE: your_email@server.domain)');
    return email.focus();
  }
  else if (prov.value == "")
  {
    alert('Please choose a Province');
    return prov.focus();
  }
  else
  {
    /* Do I get an A+? */
    alert('A+?');
  }
}
